%!TEX program = lualatex
\documentclass[aspectratio=169, 8pt, compress]{beamer}

\RequirePackage{luatex85}

\usepackage{polyglossia, csquotes}
\usepackage{pgfpages}
\usepackage{pgf}
\usepackage{fontspec}
\usepackage{unicode-math}
\usepackage{mathtools}
\usepackage{xcolor}
\usepackage{amssymb, latexsym}
\usepackage{biblatex}
\usepackage[all]{xy}
\usepackage{stmaryrd}

% Solve the problem with align and pause
\makeatletter
\let\save@measuring@true\measuring@true
\def\measuring@true{%
  \save@measuring@true
  \def\beamer@sortzero##1{\beamer@ifnextcharospec{\beamer@sortzeroread{##1}}{}}%
  \def\beamer@sortzeroread##1<##2>{}%
  \def\beamer@finalnospec{}%
}
\makeatother

\newcommand*{\umbruch}{\usebeamertemplate{theorem end} \framebreak~\usebeamertemplate{theorem begin}}
\makeatletter
\newenvironment<>{proofs}[1][\proofname]{%
    \par
    \def\insertproofname{#1\@addpunct{.}}%
    \usebeamertemplate{proof begin}#2}
  {\usebeamertemplate{proof end}}
\makeatother

\newcounter{saveenumi}
\newcommand{\saveenumerate}{%
  \stepcounter{saveenumi}%
  \label{saveenumi-\thesaveenumi}}
\newcommand{\restoreenumerate}{%
  \setcounterref{enumi}{saveenumi-\thesaveenumi}}

\definecolor{lmuGreen}{RGB}{0,148,64}
\definecolor{background}{RGB}{230,230,230}

\setbeamercolor{title}{fg=lmuGreen}
\setbeamercolor{frametitle}{fg=lmuGreen}
\setbeamercolor{structure}{fg=lmuGreen}

%\setbeameroption{show notes on second screen}

\setbeamertemplate{navigation symbols}{}
\setdefaultlanguage[spelling=new, babelshorthands=true]{german}
\setbeamercolor{background canvas}{bg=background}

\theoremstyle{remark}
\newtheorem{defn}{Definition}
\newtheorem{alg}{Algorithmus}
\newtheorem{nota}{Notation}
\newtheorem{satz}{Satz}
\newtheorem{bem}{Bemerkung}
\newtheorem{lem}{Lemma}
\newtheorem{bsp}{Beispiel}
\newtheorem{prop}{Proposition}

\makeatletter
\setbeamertemplate{theorem begin}{%
    \inserttheoremname{}
    \inserttheoremnumber{}
    \space
}
\setbeamertemplate{theorem end}{}
\makeatother

\newcommand{\opr}[1]{\operatorname{#1}}
\newcommand{\bbrack}[1]{\llbracket #1 \rrbracket}
\newcommand{\Wc}{W_{\operatorname{conv}}}
\newcommand{\lc}{l_{\operatorname{conv}}^0}
\newcommand{\trennlinie}{{\color{lmuGreen}\rule{\textwidth}{0.5pt}} \\}

\setbeamertemplate{theorems}[numbered]

\AtBeginSection[]{\begin{frame}\vfill\begin{beamercolorbox}[center]{title}\usebeamerfont{title}\insertsectionhead\par\end{beamercolorbox}\vfill \end{frame}}

\useoutertheme[]{miniframes}

\renewcommand\qedsymbol{\color{lmuGreen}{\(\blacksquare\)}}

\setsansfont{Open Sans}
\setmathfont[Scale=MatchUppercase]{Latin Modern Math}
\setmathfont[range=\setminus, Scale=MatchUppercase]{XITS Math}

\title{Äquivalenz der Approximation durch Convolutional Neural Networks und Fully-Connected Networks}
\author{Jakob Neumaier \linebreak Thomas Eingartner}
\date{02. Juli 2020}

\begin{document}
\maketitle

\begin{frame}
\begin{thebibliography}{20}
\setbeamertemplate{bibliography item}[text]
\bibitem[Coh0]{Coh0} 
P. Petersen and F. Voigtlaender.
\textit{Equivalence of approximation by convolutional neural networks and fully-connected networks}. 
Proc. Amer. Math. Soc., 2019.\end{thebibliography}
\end{frame}

\section{Fully-Connected Neural Networks}

\subsection{Definition}


\begin{frame}

\begin{bem}[Konventionen]

Für eine endliche Indexmenge $I$ sei $\mathbb{R}^I = \{(x_i)_{i \in I} | x_i \in \mathbb{R}\}$. \pause

Definiere $\llbracket M \rrbracket = \{1,...,M\}$.

\end{bem}
\pause

\begin{bem} [Affine lineare Abbildungen]
Für eine affine lineare Abbildung $V: \mathbb{R}^I \to \mathbb{R}^J$ gibt es einen eindeutigen Vektor $b \in \mathbb{R}^J$ und eine lineare Abbildung $A: \mathbb{R}^I \to \mathbb{R}^J$, so dass $V(x) = Ax+b$ für alle $x\in \mathbb{R}^I$. \pause

Definiere nun $||b||_{l^0}$ als die Anzahl der Einträge von $b$ ungleich $0$ und ebenso $||A||_{l^0}$ (bzgl. Standardbasis). \pause

Somit ist $||V||_{l^0} := ||b||_{l^0} + ||A||_{l^0}$ die Anzahl der relevanten Parameter der affinen linearen Abbildung.

\end{bem}

\end{frame}

\begin{frame}
\begin{defn}[Fully-Connected Neural Network]

Sei $\mathcal{G}$ eine endliche Gruppe, $C_0, L, N_1, ..., N_L \in \mathbb{N}$.
Ein \textit{Fully-Connected Neural Network} $\Phi := (V_1, ..., V_L)$ besteht aus affinen linearen Abbildungen. \pause

Dabei sei $V_1: \mathbb{R}^{\llbracket C_0 \rrbracket \times \mathcal{G}} \to \mathbb{R}^{N_1}$ und $V_l: \mathbb{R}^{N_{l-1}} \to \mathbb{R}^{N_l}$ für $ 1 < l \leq L$. \pause

Die \textit{Architektur} sei $\mathcal{A}(\Phi) := (C_0 \cdot |\mathcal{G}|, N_1, ..., N_L)$. \pause

Sei $\varrho: \mathbb{R} \to \mathbb{R}$ eine \textit{Aktivierungsfunktion}, dann ist 
$$R_\varrho (\Phi): \mathbb{R}^{\llbracket C_0 \rrbracket \times \mathcal{G}} \to \mathbb{R}^{N_L}, x \mapsto x_L $$
die \textit{$\varrho$-Realisation} von $\Phi$, \pause wobei $x_0 := x$ und $x_{l+1} := \varrho(V_{l+1}(x_l))$ für $ 0 \leq l \leq L-2$ und $x_L := V_L(x_{L-1})$. \pause

Insgesamt erhalten wir also folgende Anzahl an Gewichten $W(\Phi)$ und Anzahl an Neuronen $N(\Phi)$:
$$W(\Phi) := \sum_{l=1}^L ||V_l||_{l^0} \mbox{ und }  N(\Phi) := C_0 \cdot |\mathcal{G}| + \sum_{l=1}^L N_l . $$  

\end{defn}

\end{frame}



\section{Convolutional Neural Networks}
\subsection{Einführung der Idee}

\begin{frame}

\centering
\includegraphics[scale=0.4]{img/convNetwork.jpg}

\end{frame}

\subsection{Definition}

\begin{frame}

\begin{bem}[Faltung]
Sei $\mathcal{G}$ eine endliche Gruppe und $a,b:\mathcal{G} \to \mathbb{R}$ zwei Abbildungen. Die \textit{Faltung} oder \textit{Convolution} von $a$ und $b$ ist definiert als
$$a*b: \mathcal{G} \to \mathbb{R}, g \mapsto \sum_{h \in \mathcal{G}} a(h) \cdot b(h^{-1} g) . $$ 

\end{bem} \pause

\begin{bem}[Lifting]
Für eine Abbildung $F: \mathbb{R}^J \to \mathbb{R}^I$ sei das \textit{Lifting} definiert als die von $F$ induzierte Abbildung $F^\uparrow : \mathbb{R}^{J \times \mathcal{G}} \to \mathbb{R}^{I \times \mathcal{G}}$, die als Anwendung von $F$ auf $x_g \in \mathbb{R}^I$    $ \forall g \in \mathcal{G}$ entsteht. \pause

Dann folgt für $F: \mathbb{R}^J \to \mathbb{R}^I$ und $G: \mathbb{R}^K \to \mathbb{R}^J$:
$$(F \circ G)^\uparrow = F^\uparrow \circ G^\uparrow.$$
\end{bem} 

\end{frame}




\begin{frame}

\begin{defn}[Filter]
Sei $\mathcal{G}$ eine endliche Gruppe und $B: \mathbb{R}^{\llbracket C_1 \rrbracket \times \mathcal{G}} \to \mathbb{R}^{\llbracket k \rrbracket \times \llbracket C_1 \rrbracket \times \mathcal{G}}$, wobei $k, C_1 \in \mathbb{N}$. Wir nennen $B$ eine \emph{Filterung mit $k$ Filtern}, \pause wenn es $a_1, ..., a_k \in \mathbb{R}^{\mathcal{G}}$ gibt, so dass
$$B((x_i)_{i \in \llbracket C_1 \rrbracket}) = (x_j \star a_r)_{(r,j) \in \llbracket k \rrbracket \times \llbracket C_1 \rrbracket} \mbox{    } \forall (x_i)_{i \in \llbracket C_1 \rrbracket} \in \mathbb{R}^{\llbracket C_1 \rrbracket \times \mathcal{G}} $$ \pause


Nun schreiben wir $B \in \mbox{filter}(\mathcal{G}, k, C_1)$. Wir definieren $||B||_{l^0_{\mbox{\begin{tiny} filter \end{tiny}}}} := \sum_{r=1}^k ||a_r||_{l^0}$. Da $a_1,...,a_k$ eindeutig durch $B$ bestimmt sind, ist dies wohldefiniert.

\end{defn}

\end{frame}

\begin{frame}

\centering
\includegraphics[scale=0.4]{img/convNetwork.jpg}

\end{frame}

\begin{frame}

\begin{defn}[spatially-convolutional, semi-connected Abbildung]
Seien $k, C_1, C_2 \in \mathbb{N}$, dann nennen wir eine Abbildung $T: \mathbb{R}^{\llbracket C_1 \rrbracket \times \mathcal{G}} \to \mathbb{R}^{\llbracket C_2 \rrbracket \times \mathcal{G}}$ eine \textit{spatially-convolutional, semi-connected Abbildung mit $k$ Filtern}, \pause falls $T$ geschrieben werden kann als $T = A^\uparrow \circ B$, wobei $B \in \mbox{filter}(\mathcal{G}, k, C_1)$ und $A: \mathbb{R}^{\llbracket k \rrbracket \times \llbracket C_1 \rrbracket} \to \mathbb{R}^{\llbracket C_2 \rrbracket}$ eine affine lineare Abbildung sei. \pause
Nun schreiben wir $T \in \mbox{Conv}(\mathcal{G}, k, C_1, C_2)$  \pause und definieren
\[
  \| T \|_{l_{\opr{conv}}^0} 
  \coloneqq \min
  \left\{
      \|A\|_{l^0} + \|B\|_{l_{\opr{filter}^0}}
      \; \middle | \; A: \mathbb{R}^{\bbrack{C_1} \times \mathcal{G}} \longrightarrow \mathbb{R}^{\bbrack{C_2}} \text{ affin linear, } B \in \opr{filter}(\mathcal{G}, k, C_1) \text{ und } T = A^{\uparrow} \circ B
  \right\}
\]
\end{defn} \pause

\begin{bem}[Anzahl an Gewichten]
Ein $T \in \mbox{Conv}(\mathcal{G}, k, C_1, C_2)$ ist immer eine affine lineare Abbildung. \pause

Außerdem kann die Anzahl an Gewichten von $T$ als affine lineare Abbildung abgeschätzt werden mithilfe von $||T||_{l_{\mbox{{\begin{tiny} conv \end{tiny}}}}^0} $:
$$||T||_{l^0} \leq |\mathcal{G}|^2 \cdot ||T||_{l_{\mbox{{\begin{tiny} conv \end{tiny}}}}^0}$$ 
\end{bem}

\end{frame}

\begin{frame}

\centering
\includegraphics[scale=0.4]{img/convNetwork.jpg}

\end{frame}

\begin{frame}

\begin{defn}[Convolutional Neural Network]
Sei $L \in \mathbb{N}$, sei $\mathcal{G}$ eine endliche Gruppe, seien $C_0, C_1, ..., C_L \in \mathbb{N}$, seien $k_1, ..., k_L \in \mathbb{N}$. \pause

Ein \textit{Convolutional Neural Network} $\Phi$ mit $L$ layers, channel counts $(C_0, C_1, ..., C_L)$ und filter counts $(k_1, ..., k_L)$ ist ein Tupel $\Phi = (T_1, ..., T_L)$, wobei jeweils $T_l \in \mbox{Conv}(\mathcal{G}, k_l, C_{l-1}, C_l)$. \pause

Für ein CNN $\Phi$ und eine Aktivierungsfunktion $\varrho: \mathbb{R} \to \mathbb{R}$ definieren wir die \textit{$\varrho$-Realisierung} von $\Phi$ als
$$R_\varrho : \mathbb{R}^{\llbracket C_0 \rrbracket \times \mathcal{G}} \to \mathbb{R}^{\llbracket C_L \rrbracket \times \mathcal{G}}, x \mapsto x_L, $$
wobei $x_0 := x, x_{l+1} := \varrho(T_{l+1}(x_l))$ für $0 \leq l \leq L-2$, und $x_L := T_L(x_{L-1})$. \pause

Insgesamt ist also die Anzahl der Channel $C(\Phi) := \sum_{l=0}^L C_l$. Dabei nennen wir $C_0 = C_0(\Phi)$ die Anzahl der Input-Channel von $\Phi$, $C_L = C_L(\Phi)$ ist hingegen die Anzahl der Output-Channel.  \pause

Die Anzahl an Gewichten ist $W_{\mbox{{\begin{tiny} conv \end{tiny}}}}(\Phi) := \sum_{l=0}^L ||T_l||_{l_{\mbox{{\begin{tiny} conv \end{tiny}}}}^0}$.
\end{defn}

\end{frame}


\begin{frame}

\begin{bem}[CNNs als spezielle FNNs]
Jedes CNN $\Phi = (T_1, ..., T_L)$ ist auch ein FNN, da jede der Abbildungen $T_l \in \mbox{Conv}(\mathcal{G}, k, C_{l-1}, C_l)$ eine affine lineare Abbildung $T_l: \ \mathbb{R}^{\llbracket C_{l-1} \rrbracket \times \mathcal{G}} \to \mathbb{R}^{\llbracket C_l \rrbracket \times \mathcal{G}}$ ist. \pause

Somit hat $\Phi$ als FNN die Architektur $\mathcal{A}(\Phi) := (C_0 \cdot |\mathcal{G}|, C_1 \cdot |\mathcal{G}|, ..., C_L \cdot | \mathcal{G}|)$ und somit $N(\Phi) = |\mathcal{G}| \cdot C(\Phi)$. \pause

Aus der Abschätzung $||T||_{l^0} \leq |\mathcal{G}|^2 \cdot ||T||_{l_{\mbox{{\begin{tiny} conv \end{tiny}}}}^0}$ (Bemerkung 5) folgt 
$$ W(\Phi) = \sum_{l=0}^L ||T_l||_{l^0} \leq |\mathcal{G}|^2 \cdot \sum_{l=0}^L ||T_l||_{l_{\mbox{{\begin{tiny} conv \end{tiny}}}}^0} = |\mathcal{G}|^2 \cdot W_{\mbox{{\begin{tiny} conv \end{tiny}}}}(\Phi)$$ \pause

Somit sind CNNs also eine spezielle Form der FNNs.

CNNs sind jedoch nur in der Lage, translationsequivariante Funktionen zu approximieren.
\end{bem}
\end{frame}




\section{Translationsäquivarianz}

\begin{frame}

\begin{bem}[I-Vektorisierung]

Für eine Menge $I$ und eine Funktion $H: \mathbb{R}^{\mathcal{G}} \to \mathbb{R}^{\mathcal{G}}$ definieren wir die \textit{$I$-Vektorisierung} $H^{{\tiny \mbox{vec}}, I}$ als

$$ H^{{\tiny \mbox{vec}}, I}: \mathbb{R}^{I \times \mathcal{G}} \to \mathbb{R}^{I \times \mathcal{G}}, (x_i)_{i \in I} \mapsto (H(x_i))_{i \in I}. $$

\end{bem} \pause

\begin{defn}[Translationsäquivarianz]

Sei $\mathcal{G}$ eine endliche Gruppe, $I, J$ Indexmengen, $F: \mathbb{R}^{I \times \mathcal{G}} \to \mathbb{R}^{J \times \mathcal{G}}$. Dann nennen wir F \textit{translationsäquivariant}, falls $F \circ S_g^{{\tiny \mbox{vec}}, I} = S_g^{{\tiny \mbox{vec}}, J} \circ F$ für alle $g \in \mathcal{G}$, wobei $S_g: \mathbb{R}^{\mathcal{G}} \to \mathbb{R}^{\mathcal{G}}, (x_h)_{h \in \mathcal{G}} \mapsto (x_{g^{-1} h})_{h \in \mathcal{G}}$ der \textit{Shift-Operator} sei. \pause

\end{defn}

\begin{satz}
Sei $\mathcal{G}$ eine endliche Gruppe, sei $\varrho: \mathbb{R} \to \mathbb{R}$ eine Abbildung und sei $\Phi$ ein CNN.


Dann ist die \textit{$\varrho$-Realisierung} $R_\varrho (\Phi)$ translationsequivariant. \label{CnnRealisierungTranslqvariant}
\end{satz}

\end{frame}


\section{Transference Principle}

\begin{frame}
  \begin{nota}[Projektion auf \(g\)-te Komponente] \pause{}
    \[\pi_g^J: \ \mathbb{R}^{J \times \mathcal{G}} \longmapsto \mathbb{R}^{J}, \ (x_{j,h})_{j \in J, h \in \mathcal{G}} \longmapsto (x_{j, g})_{j \in J}\]\label{definitionProjektion}\pause{}
  \end{nota}

  \begin{defn}[\(\mathcal{G}\)-invariante Mengen]\pause{}
    Sei \(I \ne \emptyset\) eine Indexmenge. \pause{} \(\Omega \subseteq \mathbb{R}^{I \times \mathcal{G}}\) heißt \emph{\(\mathcal{G}\)-invariant} falls \(S_g^{\opr{vec}, I}(\Omega) \subseteq \Omega\) für alle \(g \in \mathcal{G}\). \pause{}
  \end{defn}

  \begin{bsp}\pause{}
    Sei \(\Omega_i \subseteq \mathbb{R}\), dann ist \(\prod_{i \in I} \Omega_i^{\mathcal{G}}\) eine \(\mathcal{G}\)-invariante Menge.
  \end{bsp}
\end{frame}

\begin{frame}
  \begin{nota}[\(L^p(\Omega, \mathbb{R}^J)\)]
    Sei \(d \in \mathbb{N}\), \(J\) eine endliche Indexmenge, \(\Omega \subseteq \mathbb{R}^d\) messbar, \(p \in (0, \infty]\) und \(f: \Omega \to \mathbb{R}^J\) messbar. \pause{}Dann schreiben wir 
    \begin{equation*}
      \|f\|_{L^p(\Omega, \mathbb{R}^J)} \coloneqq \left\| x \longmapsto \| f(x)\|_{l^p}\right\|_{L^p(\Omega)}.
    \end{equation*}\label{LpNormNota}\pause{}
  \end{nota}

  \begin{bem}[Translationsäquiv.\ Abb.\ mit einer Komponente eindeutig bestimmt]\pause{}
    Sei \(1 \in \mathcal{G}\) das neutrale Element von \(\mathcal{G}\) \pause{}und seien \(F, G: \mathbb{R}^{I \times \mathcal{G}} \to \mathbb{R}^{J \times \mathcal{G}}\) translationsäquivariant. \pause{}Gilt \((F)_1 = (G)_1\) dann folgt \(F = G\)\pause{}, denn\label{Reicht1zuzeigen}
    \begin{align*}
      (F)_g \pause{} 
      &= \pi_1^J \circ S_{g^{-1}}^{\opr{vec}, J} \circ F \pause{}
      \overset{(F \text{ translationsäquivariant})}{=} \pi_1^J \circ F \circ S_{g^{-1}}^{\opr{vec}, I} \pause{}
      \overset{(\text{Def. } \pi)}{=} (F)_1 \circ S_{g^{-1}}^{\opr{vec}, I} \pause{} \\
      &= (G)_1 \circ S_{g^{-1}}^{\opr{vec}, I} \pause{}
      \overset{(\text{Def. } \pi)}{=} \pi_1^J \circ G \circ S_{g^{-1}}^{\opr{vec}, I} \pause{}
      \overset{(G \text{ translationsäquivariant})}{=} \pi_1^J \circ S_{g^{-1}}^{\opr{vec}, J} \circ G \pause{}
      = (G)_g.
    \end{align*}\pause{}
  \end{bem}

  \begin{bem}\pause{}
    Aus Notation~\ref{LpNormNota} und aus Bemerkung~\ref{Reicht1zuzeigen} folgt für \(F, G : \mathbb{R}^{I \times \mathcal{G}} \to \mathbb{R}^{J \times \mathcal{G}}\) translationsäquivariant:\pause{}
    \begin{equation}
      \|F - G\|_{L^p(\Omega, \mathbb{R}^{J \times G})} \pause{}= \left( \sum_{g \in \mathcal{G}} \|(F)_g - (G)_g \|_{L^p(\Omega, \mathbb{R}^J)}^p \right)^{\frac{1}{p}} \pause{}= |\mathcal{G}|^{\frac{1}{p}} \cdot \|(F)_1 - (G)_1\|_{L^p(\Omega, \mathbb{R}^J)}, 
      \quad p \in (0, \infty].
    \end{equation}\label{translquvrtReichtEinPUnkt}
  \end{bem}
\end{frame}

\section{Äquivalenz von FNNs und CNNs}
\begin{frame}
  \begin{satz}
    Sei \(\mathcal{G}\) eine endliche Gruppe, \pause{} \(\epsilon \in [0, \infty)\), \pause{} \(p \in (0, \infty]\) \pause{} und \(C_0, N \in \mathbb{N}\). \pause{} Es sei \(\rho: \mathbb{R} \to \mathbb{R}\) messbar \pause{}und \(\Omega \subseteq \mathbb{R}^{\llbracket C_0\rrbracket  \times \mathcal{G}}\) eine \(\mathcal{G}\)-invariante Menge. \pause{}% chktex 9 
    Sei \(F:\ \mathbb{R}^{\llbracket C_0\rrbracket  \times \mathcal{G}} \to \mathbb{R}^{\llbracket N\rrbracket  \times \mathcal{G}}\) messbar und translationsäquivariant \pause{} und \(\Phi\) ein FNN mit Architektur \(\mathcal{A}(\Phi) = (C_0 \times |\mathcal{G}|, N_1, \dots, N_{L-1}, N)\) sodass \(\|(F)_1 - R_\rho(\Phi)\|_{L^p(\Omega, \mathbb{R}^N)} \le \epsilon\). \pause{}\\ 
    Dann existiert ein CNN \(\Psi\) mit der Anzahl von Kanälen \((C_0, N_1, \dots, N_{L-1}, N)\) und Filtern \((N_1 \cdot C_0, 1, \dots, 1)\) \pause{}sodass \(\|F-R_\rho(\Psi)\|_{L^p(\Omega, \mathbb{R}^{\llbracket N\rrbracket  \times \mathcal{G}})} \le |\mathcal{G}|^{1/p} \cdot \epsilon\) \pause{}und \(\Wc(\Psi) \le 2 \cdot W(\Phi)\). \pause{} Dabei gilt die Konvention \(|G|^{1/\infty} = 1\).
  \end{satz}\pause{}

  \begin{proofs}\pause{}
    Da für ein CNN \(\Psi\) gilt, dass \(R_\rho(\Psi)\) translationsäquivariant ist reicht es nach Bemerkung~\ref{translquvrtReichtEinPUnkt} zu zeigen, dass es ein \(\Psi\) mit den geforderten Kriterien gibt sodass \((R_\rho(\Psi))_1 = R_\rho(\Phi)\).\pause{} Dann folgt nämlich
    \[
      \|F-R_\rho(\Psi)\|_{L^p(\Omega, \mathbb{R}^{\llbracket N\rrbracket  \times \mathcal{G}})} \pause{} 
      \overset{(\text{Bem.~\ref{translquvrtReichtEinPUnkt}})}{=} |\mathcal{G}|^{\frac{1}{p}} \cdot \|(F)_1 - (R_\rho(\Psi))_1\|_{L^p(\Omega, \mathbb{R}^N)} \pause{}
      = |\mathcal{G}|^{\frac{1}{p}} \cdot \| (F)_1 - R_\rho(\Phi)\|_{L^p(\Omega, \mathbb{R}^N)} \pause{}
      \overset{(\text{Vor.})}{\le} |\mathcal{G}|^{\frac{1}{p}} \cdot \epsilon
    \]
    Sei \(\Phi = (V_1, \dots, V_L)\). \pause{}Wir setzen \(N_0 \coloneqq C_0\), \pause{}\(N_L \coloneqq N\), \pause{}\(k_1 \coloneqq N_1 \cdot C_0\) \pause{}und \(k_l = 1\) für alle \(l \in \{2, \dots, L\}\).
  \end{proofs}
\end{frame}
  
\begin{frame}
  Wir betrachten zuerst den Sonderfall \(\|V_L\|_{l^0} = 0\). \pause{}Dann gilt \(R_\rho(\Phi) = 0\). \pause{}Setze nun \(\Psi = (T_1, \dots, T_L)\)\pause{}, wobei \(T_l = A_l^\uparrow \circ B_l\) für alle \(l \in \{1, \dots, L\}\) \pause{}und
  \begin{align*}
    &A_l: \ \mathbb{R}^{\llbracket k_l\rrbracket \times \llbracket N_{l-1}\rrbracket } \longrightarrow \mathbb{R}^{\llbracket N_l\rrbracket }, \ x \longmapsto 0 \\
    &B_l: \ \mathbb{R}^{\llbracket N_{l-1}\rrbracket  \times \mathcal{G}} \longrightarrow \mathbb{R}^{\llbracket k_l\rrbracket  \times \llbracket N_{l-1}\rrbracket  \times \mathcal{G}}, \ (x_i)_{i \in \llbracket N_{l-1}\rrbracket } \longmapsto (x_i \star 0)_{r \in \llbracket k_l\rrbracket , i \in \llbracket N_{l-1}\rrbracket }.
  \end{align*}\pause{}
  Dann besitzt \(\Psi\) die geforderte Anzahl an Filtern und Kanälen, \pause{}es gilt \((R_\rho(\Psi))_1 = 0\) \pause{}und \(\|T_l\|_{\lc} = 0\) für alle \(l = 1, \dots, L\), \pause{}sodass \(\Wc(\Psi)  = 0 \le 2 W(\Phi) \).
\end{frame}

\begin{frame}
  Als nächstes betrachten wir den Fall, dass \(\| V_L \|_{l^0} > 0\), aber \(\|V_l \|_{l^0} = 0\) für ein \(l \in \{1, \dots, L-1\}\). \pause{}Dann gibt es ein \(c \in \mathbb{R}^N\), sodass \(\| c \|_{l_0} \le W(\Phi)\) und \(R_\rho (\Phi) = c\) \pause{}und es gibt ein \(c_0 \in R^{N_{L-1}}\) mit \(c = V_Lc_0\). \pause{}\\
  Für jedes \(A \in \mathbb{R}^{n \times k}, b \in \mathbb{R}^n\) und \(x \in \mathbb{R}^k\) \pause{} sehen wir aus \((Ax + b)_j = b_j + \sum_{l=1}^k A_{j, l} x_l\)\pause{}, dass 
  \begin{equation}
  \mathbb{1}_{(Ax + b)_j \ne 0} \le \mathbb{1}_{b_j \ne 0} + \sum_{l=1}^k \mathbb{1}_{A_{j,l} \ne 0}\label{dreicksungleichungIndikator}
  \end{equation}
  \pause{}und es folgt
  \begin{equation}
    \| Ax + b \|_{l^0} 
    \overset{(\text{Def. }l^0)}{=} \sum_{j=1}^n \mathbb{1}_{(Ax + b)_j \ne 0} \pause{} 
    \overset{(\ref{dreicksungleichungIndikator})}{\le} \sum_{j=1}^n \mathbb{1}_{b_j \ne 0} + \sum_{j=1}^n \sum_{l=1}^k \mathbb{1}_{A_{j, l} \ne 0} \pause{} 
    \overset{(\text{Def. }l^0)}{=} \| A(\cdot) + b\|_{l^0}.\label{abschaetzungL0Affine}
  \end{equation}
  \pause{}
  Also folgt
  \begin{equation}
    \|c\|_{l^0} = \| V_L c_0 \|_{l^0} \pause{} 
    \overset{(\ref{abschaetzungL0Affine})}{\le} \| V_L \|_{l^0} \pause{} 
    \le \sum_{l=1}^L \|V_l\|_{l^0} \pause{} 
    = W(\Phi). \label{cAbschaetzung}
  \end{equation}

  \pause{}Ist nun so ein Vektor \(c \in \mathbb{R}^N\) mit \(R_\rho(\Phi) = c\) und \(\| c \|_{l^0} \le W(\Phi)\) gegeben, \pause{}setze \(\Psi = (T_1, \dots, T_L)\)\pause{}, wobei \(T_l = A_l^\uparrow \circ B_l\) \pause{}mit \(B_1, \dots, B_L\) und \(A_1, \dots, A_{L-1}\) wie im vorherigen Fall \pause{}und 
  \[A_L: \ \mathbb{R}^{\llbracket k_L \rrbracket \times \llbracket N_{L-1} \rrbracket} \longrightarrow \mathbb{R}^{\llbracket N_L \rrbracket}, \ x \longmapsto c.\]\pause{}
  Dies ist wohldefiniert da \(N_L = N\) und \(c \in \mathbb{R}^N \simeq \mathbb{R}^{\llbracket N_L \rrbracket}\). \pause{} Es folgt \((R_\rho(\Psi))_1 = c = R_\rho(\Phi)\) \pause{}und \(\Psi\) hat die geforderte Anzahl an Filtern und Kanälen \pause{}und es gilt \(\Wc(\Psi)\pause{} =  \| A_L \|_{l^0} \pause{}=  \| c \|_{l^0} \pause{} \overset{(\ref{cAbschaetzung})}{\le} W(\Phi)\).
\end{frame}

\begin{frame}
  Wir können nun \(\|V_l\|_{l^0} > 0\) für alle \(l \in \{ 1, \dots, L\}\) annehmen. \pause{}Definiere nun \(v_g^\star \coloneqq v_{g^{-1}} \in \mathbb{R}^{\mathcal{G}}\) für \(v \in \mathbb{R}^{\mathcal{G}}\), \(g \in \mathbb{G}\), \pause{}dann gilt:
  \begin{equation}
    (x \star v^\star)_1
    = \sum_{h \in \mathbb{G}} x_h v_{h^{-1} \cdot 1}^\star \pause{}
    = \sum_{h \in \mathbb{G}} x_h v_{h^{-1}}^\star \pause{}
    = \sum_{h \in \mathbb{G}} x_h v_h \pause{}
    = \langle x, v \rangle_{\mathbb{R}^{\mathbb{G}}} 
    \qquad \forall x \in \mathbb{R}^{\mathbb{G}} \label{faltScalarGleich}
  \end{equation} \pause{}
  Sei nun \((\delta_1)_g = \begin{cases} 1 &g = 1 \\ 0 & g \in \mathcal{G} \setminus \{1\} \end{cases}\) \pause{}und \(x \in \mathbb{R}^{\mathcal{G}}\). \pause{}Dann gilt
  \begin{equation}
  (x \star \delta_1)_g \pause{}
  = \sum_{h \in \mathcal{G}} x_h (\delta_1)_{h^{-1} g} \pause{}
  = x_g (\delta_1)_{g^{-1} g} \pause{}
  = x_g, \label{delta1istidentitaet}
  \end{equation}\pause{}
  also \((x \star \delta_1) = x\).
\end{frame}

\begin{frame}
  Es gelte nach wie vor 
  \(\Phi = (V_1, \dots, V_L)\). \pause{}Da \(V_1:\ \mathbb{R}^{\llbracket C_0 \rrbracket \times \mathcal{G}} \to \mathbb{R}^{\llbracket N_1 \rrbracket}\) affin linear ist, \pause{} gibt es \(v_j^i \in \mathbb{R}^{\mathcal{G}}\) \pause{}und \(b_j \in \mathbb{R}, j \in \llbracket N_1 \rrbracket, i \in \llbracket C_0 \rrbracket\), \pause{}sodass \(V_1(\cdot) = b + V_1^{\operatorname{lin}}\), wobei \(b = (b_j)_{j \in \llbracket N_1 \rrbracket}\) \pause{}und 
  \[V_1^{\operatorname{lin}}: \ \mathbb{R}^{\llbracket C_0 \rrbracket \times \mathcal{G}} \longrightarrow \mathbb{R}^{\llbracket N_1 \rrbracket}, \ (x_i)_{i \in \llbracket C_0 \rrbracket} \longmapsto \left(\sum_{i=1}^{C_0} \langle x_i, v_j^i\rangle_{\mathbb{R}^{\mathcal{G}}} \right)_{j \in \llbracket N_1 \rrbracket}\]\pause{}
  Definiere nun \(T_1 \coloneqq A_1^\uparrow \circ B_1 \in \opr{Conv}(\mathcal{G}, C_0 \cdot N_1, C_0, N_1)\) \pause{}mit
  \[B_1: \ \mathbb{R}^{\llbracket{C_0} \rrbracket \times \mathcal{G}} \longrightarrow \mathbb{R}^{\llbracket N_1 \rrbracket \times \llbracket C_0 \rrbracket \times \llbracket C_0 \rrbracket \times \mathcal{G}}, \ (x_i)_{i \in \llbracket C_0 \rrbracket} \longmapsto (x_\iota \star (v_j^i)^\star)_{j \in \bbrack{N_1}, i, \iota \in \bbrack{C_0}}\]\pause{}
  und \(A_1: \ \mathbb{R}^{\llbracket N_1 \rrbracket \times \llbracket C_0 \rrbracket \times \llbracket C_0 \rrbracket} \longrightarrow \mathbb{R}^{\llbracket N_1 \rrbracket}, \ y \longmapsto b + A_1^{\opr{lin}} y\), \pause{}wobei
  \[
    A_1^{\opr{lin}}: \ \mathbb{R}^{\bbrack{N_1} \times \bbrack {C_0} \times \bbrack{C_0}} 
    \longrightarrow \mathbb{R}^{\bbrack{N_1}}, \ (y_{j,i,\iota})_{j \in \bbrack{N_1}; i,\iota \in \bbrack{C_0}} 
    \longmapsto \left( \sum_{i=1}^{C_0} \mathbb{1}_{v_l^i \ne 0} \cdot y_{l, i, i} \right)_{l \in \bbrack{N_1}}.
  \] \pause{}

  Sei nun \(x = (x_{j, g})_{j \in \bbrack{C_0}, g \in \mathcal{G}} \in \mathbb{R}^{\bbrack{C_0} \times \mathcal{G}}\) \pause{}und \(l \in \bbrack{N_1}\), \pause{}dann:

  {\small
    \begin{align*}
      (T_1 x)_{l, 1}
      = \pause{}((A_1^\uparrow \circ B_1)(x)_{l, 1} 
      &= \pause{}\left(A_1 \left[(B_1 x)_{j, i, \iota, 1}\right]_{(j, i, \iota) \in \bbrack{N_1} \times \bbrack{C_0} \times \bbrack{C_0}}\right)_l = \pause{}\left( A_1 \left[ (x_\iota \star (v_j^i)^\star_1 \right]_{(j, i, \iota) \in \bbrack{N_1} \times \bbrack{C_0} \times \bbrack{C_0}} \right)_l \pause{} \\ % chktex 9
      \text{(Def.~von \(A_1\) und Gleichung~\ref{faltScalarGleich})} \pause{}\quad 
      &= b_l + \sum_{i=1}^{C_0} \left[ \langle x_i, v_l^i\rangle_{\mathbb{R}^{\mathcal{G}}} \cdot \mathbb{1}_{v_l^i \ne 0} \right] \pause{}= b_l + \sum_{i=1}^{C_0} \langle x_i, v_l^i \rangle_{\mathbb{R}^{\mathcal{G}}} \pause{}
      = (V_1 x)_l
    \end{align*}
  }
\end{frame}
 
\begin{frame}
  %ToDo: Definition Projektion (paper: Equation 10)
  Mit der Definition der Projektionsabbildung \(\pi_1^{\bbrack{N_1}}\) (Notation~\ref{definitionProjektion}) erhalten wir:
  \begin{equation}
    \pi_1^{\bbrack{N_1}} \circ T_1 = V_1 \label{T1projeziertIstV1}
  \end{equation}\pause{}

  \trennlinie{}
  
  \begin{itemize}
    \item \(A_1^{\opr{lin}}: \ \mathbb{R}^{\bbrack{N_1} \times \bbrack {C_0} \times \bbrack{C_0}} \longrightarrow \mathbb{R}^{\bbrack{N_1}}, \ (y_{j,i,\iota})_{j \in \bbrack{N_1}; i,\iota \in \bbrack{C_0}} \longmapsto \left( \sum_{i=1}^{C_0} \mathbb{1}_{v_l^i \ne 0} \cdot y_{l, i, i} \right)_{l \in \bbrack{N_1}}\)
  \end{itemize}
  \trennlinie{}\pause{}

  Dann gilt für alle \(j, l \in \bbrack{N_1}\) und \(i, \iota \in \bbrack{C_0}\): \pause{}

  \begin{equation}
    (A_1^{\opr{lin}} \delta_{j, i, \iota})_l \pause{}= \sum_{n=1}^{C_0} (\mathbb{1}_{v_l^n \ne 0} \cdot \delta_{j, i, \iota})_{l, n, n} \pause{}=  \Delta_{i, \iota} \cdot \mathbb{1}_{v_j^i \ne 0} \cdot \Delta_{j, l}, \quad \text{wobei }\Delta_{i,j} \coloneqq \begin{cases} 1, &i = j \\ 0, &i \ne j \end{cases}\label{einhsvecundkronecker}
  \end{equation} \pause{}

  Also gilt:
  \begin{align}
    \| A_1^{\opr{lin}} \|_{l^0} \pause{}
    \overset{\text{(Def. \(l^0\)})}{=} \sum_{j=1}^{N_1} \sum_{l=1}^{N_1} \sum_{i, \iota \in \bbrack{C_0}} \mathbb{1}_{(A_1^{\opr{lin}} \delta_{j, i, \iota})_l \ne 0} \pause{}
    \overset{(\ref{einhsvecundkronecker})}{=} \sum_{j=1}^{N_1} \sum_{i=1}^{C_0} \mathbb{1}_{v_j^i \ne 0} \pause{}
    \le \sum_{j=1}^{N_1} \sum_{j=1}^{C_0} \| v_j^i \|_{l^0} \pause{}
    \overset{\text{(Def. \(l^0\)})}{=} \| V_1^{\opr{lin}} \|_{l^0} \label{A1lingeV1lin}
  \end{align}\pause{}

  und es folgt
  \begin{align}
    \|A_1\|_{l^0} \pause{}
    \overset{\text{(Def. \(A_1\)})}{=} \|b + A_1^{\opr{lin}} y\|_{l^0} \pause{}
    \overset{\text{(Def. \(l^0\)})}{=} \|b\|_{l^0} + \|A_1^{\opr{lin}}\|_{l^0} \pause{}
    \overset{(\ref{A1lingeV1lin})}{\le} \|b\|_{l^0} + \|V_1^{\opr{lin}}\|_{l^0} \pause{}
    \overset{\text{(Def. \(l^0\)})}{=} \|b + V_1^{\opr{lin}}\|_{l^0} \pause{}\overset{\text{(Def. \(V_1\)})}{=} \|V_1\|_{l^0} \label{A1leV1}
  \end{align}
\end{frame}

\begin{frame}
  \begin{itemize}
    \item (\ref{A1leV1}) \(\|A_1\|_{l^0} \le \|V_1\|_{l^0}\)
    \item \(T_1  = A_1^\uparrow \circ B_1 \in \opr{Conv}(\mathcal{G}, C_0 \cdot N_1, C_0, N_1)\)
    \item \(A_1: \ \mathbb{R}^{\llbracket N_1 \rrbracket \times \llbracket C_0 \rrbracket \times \llbracket C_0 \rrbracket} \longrightarrow \mathbb{R}^{\llbracket N_1 \rrbracket}, \ y \longmapsto b + A_1^{\opr{lin}} y\)
    \item \(A_1^{\opr{lin}}: \ \mathbb{R}^{\bbrack{N_1} \times \bbrack {C_0} \times \bbrack{C_0}} \longrightarrow \mathbb{R}^{\bbrack{N_1}}, \ (y_{j,i,\iota})_{j \in \bbrack{N_1}; i,\iota \in \bbrack{C_0}} \longmapsto \left( \sum_{i=1}^{C_0} \mathbb{1}_{v_l^i \ne 0} \cdot y_{l, i, i} \right)_{l \in \bbrack{N_1}}\)
    \item \(B_1: \ \mathbb{R}^{\llbracket{C_0} \rrbracket \times \mathcal{G}} \longrightarrow \mathbb{R}^{\llbracket N_1 \rrbracket \times \llbracket C_0 \rrbracket \times \llbracket C_0 \rrbracket \times \mathcal{G}}, \ (x_i)_{i \in \llbracket C_0 \rrbracket} \longmapsto (x_\iota \star (v_j^i)^\star)_{j \in \bbrack{N_1}, i, \iota \in \bbrack{C_0}}\)
    \item \(V_1^{\operatorname{lin}}: \ \mathbb{R}^{\llbracket C_0 \rrbracket \times \mathcal{G}} \longrightarrow \mathbb{R}^{\llbracket N_1 \rrbracket}, \ (x_i)_{i \in \llbracket C_0 \rrbracket} \longmapsto \left(\sum_{i=1}^{C_0} \langle x_i, v_j^i\rangle_{\mathbb{R}^{\mathcal{G}}} \right)_{j \in \llbracket N_1 \rrbracket}\)
  \end{itemize}
    
  \trennlinie{}
  \pause{}
  Als nächstes stellen wir fest, dass 
  \begin{equation}
    \|B_1\|_{l_{\opr{filter}}^0} \pause{}
    \overset{\text{(Def. \(l^0_{\opr{filter}}\))}}{=} \sum_{j=1}^{N_1} \sum_{i=1}^{C_0} \|(v_j^i)^\star\|_{l^0} \pause{}
    \overset{\text{(Def. \(l^0\)})}{=} \|V_1^{\opr{lin}}\|_{l^0} \pause{}
    \le \|b + V_1^{\opr{lin}}(\cdot)\|_{l^0} \pause{}
    = \|V_1\|_{l^0}.\label{B1leV1}
  \end{equation} \pause{}

  Dies impliziert
  \begin{equation}
    \|T_1\|_{l_{\opr{conv}}^0} \pause{}
    \overset{(\|\cdot\|_{l^0_{\opr{conv}}} = \min \{ \dots \})}{\le} \|A_1\|_{l^0} + \|B_1\|_{l_{\opr{filter}}^0} \pause{}
    \overset{(\ref{A1leV1}) (\ref{B1leV1})}{\le} 2 \cdot \|V_1\|_{l^0}. \label{T1leV1}
  \end{equation}
\end{frame}

\begin{frame}
  \begin{itemize}
    \item[(\ref{T1leV1})] \(\|T_1\|_{l_{\opr{conv}}^0} \le 2 \cdot \|V_1\|_{l^0}\)
    \item \(T_1  =  A_1^\uparrow \circ B_1 \in \opr{Conv}(\mathcal{G}, C_0 \cdot N_1, C_0, N_1)\)
    \item \(\Phi = (V_1, \dots, V_L)\)
    \item \((\delta_1)_g = \begin{cases} 1 &g = 1 \\ 0 & g \in \mathcal{G} \setminus \{1\} \end{cases}\)
    \item \(x \in \mathbb{R}^{\mathcal{G}} \Rightarrow (x \star \delta_1) = x\).
  \end{itemize}
  \trennlinie{} \pause{}
  
  Für \(l \in \{2, \dots, L\}\) \pause{}definiere nun \(T_l \coloneqq V_l^\uparrow \circ B_l \in \opr{Conv}(\mathcal{G}, 1, N_{l-1}, N_l)\) \pause{}mit
  \[
    B_l: \ \mathbb{R}^{\bbrack{N_{l-1}} \times \mathcal{G}} \longmapsto \mathbb{R}^{\bbrack{N_{l-1}} \times \mathcal{G}}, \ 
    x = (x_i)_{i \in \bbrack{N_{l-1}}} \longmapsto (x_i \star \delta_1)_{i \in \bbrack{N_{l-1}}} \overset{(\ref{delta1istidentitaet})}{=} x.
  \]\pause{}

  Wegen \(B_l x = x\) gilt \(T_l = V_l^\uparrow\). \pause{}Da wir den Fall \(\|V_l\|_{l^0} \ne 0\) betrachten gilt \(\|B_l\|_{l_{\opr{filter}}^0} \overset{(\text{Def. }l_{\opr{filter}}^0)}{=} \|\delta_1\|_{l^0} = 1 \le ||V_l\|_{l^0}\).\pause{}

  \vfill

  Wir können nun das CNN \(\Psi \coloneqq (T_1, \dots, T_L)\) definieren. \pause{}Dann hat \(\Psi\) die Anzahl von Kanälen \((C_0, N_1, \dots, N_{L-1}, N)\) und Filtern \((N_1 \cdot C_0, 1, \dots, 1)\) \pause{}und es gilt
  \[
    \Wc (\Psi) \pause{}
    \overset{(\text{Def. }\Wc)}{=} \sum_{l=1}^L \| T_l \|_{l_{\opr{conv}}^0} \pause{}
    \overset{(\ref{T1leV1})}{\le} 2 \sum_{l=1}^L \|V_l\|_{l^0} \pause{}
    \overset{(\text{Def. \(W\)})}{=} 2 W(\Phi)
  \]
\end{frame}

\begin{frame}
  \begin{itemize}
    \item[(\ref{T1projeziertIstV1})] \(\pi_1^{\bbrack{N_1}} \circ T_1 = V_1\)
  \end{itemize}
  \trennlinie{}
  \pause{}

  Wir haben bereits gezeigt, dass \(T_l = V_l^\uparrow\) für \(l \in \{2, \dots, L\}\). \pause{}Hieraus folgt 
  \begin{equation}
    \pi_1^{\bbrack{N_l}} \circ T_l \pause{}
    = \pi_1^{\bbrack{N_L}} \circ V_l^\uparrow \pause{}
    = V_l \circ \pi_1^{\bbrack{N_{l-1}}}. \label{TlisVl}
  \end{equation} \pause{}

  Da die Aktivierungsfunktion \(\rho\) komponentenweise angewandt wird können wir schließen:\pause{}
  \begin{align*}
    [R_\rho(\Psi)]_1 
    &= \pi_1^{\bbrack{N_L}} \circ R_\rho(\Psi) \pause{} \\
    &= \pi_1^{\bbrack{N_L}} \circ (T_L \circ \rho \circ T_{L-1} \circ \dots \circ \rho \circ T_1) \pause{} \\
    (\ref{TlisVl}) \quad
    &= (V_L \circ \rho \circ V_{L-1} \circ \dots \rho \circ V_2) \circ \pi_1^{\bbrack{N_1}} \circ \rho \circ T_1 \\ \pause{}
    \text{((\ref{T1projeziertIstV1}) und \(\pi_1^{\bbrack{N_1}}\circ \rho = \rho \circ \pi_1^{\bbrack{N_1}}\))} \pause{} \quad 
    &= (V_L \circ \rho \circ V_{L-1} \circ \dots \circ \rho \circ V_2) \circ \rho \circ V_1 \\ \pause{}
    &= R_\rho(V_1, \dots, V_L) \pause{} \\
    &= R_\rho(\Phi)
  \end{align*}\pause{}

  Hieraus folgt die Behauptung. \qed\
\end{frame}

\subsection{}
\begin{frame}
  \centering{}
  Danke.  
\end{frame}

%ToDo?: Remark auf 1577


\end{document}