\documentclass[review,onefignum,onetabnum]{siamart190516}


% \usepackage{unicode-math}
\usepackage{mathtools}
% \usepackage{xcolor}
\usepackage{amssymb, latexsym}
\usepackage{stmaryrd}

\newcommand{\opr}[1]{\operatorname{#1}}
\newcommand{\bbrack}[1]{\llbracket #1 \rrbracket}
\newcommand{\Wc}{W_{\operatorname{conv}}}
\newcommand{\lc}{l_{\operatorname{conv}}^0}


% Information that is shared between the article and the supplement
% (title and author information, macros, packages, etc.) goes into
% ex_shared.tex. If there is no supplement, this file can be included
% directly.

\input{ex_shared}

% Optional PDF information
\ifpdf
\hypersetup{
  pdftitle={Äquivalenz von Fully Connected Neural Networks und Convolutional Neural Networks im Sinne der Approximierbarkeit translationsinvarianter Funktionen},
  pdfauthor={T. Eingartner}
}
\fi

% The next statement enables references to information in the
% supplement. See the xr-hyperref package for details.

\externaldocument{ex_supplement}

% FundRef data to be entered by SIAM
%<funding-group specific-use="FundRef">
%<award-group>
%<funding-source>
%<named-content content-type="funder-name"> 
%</named-content> 
%<named-content content-type="funder-identifier"> 
%</named-content>
%</funding-source>
%<award-id> </award-id>
%</award-group>
%</funding-group>

\begin{document}

\title{Äquivalenz von Fully Connected Neural Networks und Convolutional Neural Networks im Sinne der Approximierbarkeit translationsinvarianter Funktionen}
\author{T. Eingartner}
\maketitle

% REQUIRED
\begin{abstract}
  Convolutional Neural Networks (CNNs) spielen in der Anwendung von Machine Learning Techniken eine dominante Rolle. Die Mathematik befasst sich bisher jedoch primär mit Fully Connected Neural Networks (FNN). Um bereits vorhandene Theorie für FNNs auf CNNs übertragen zu können und Theorien zu verbinden zeigen wir das beide Netzwerktypen im Sinne der Approxierbarkeit von translationsinvarianten Funktionen äquivalent sind.
\end{abstract}

% REQUIRED
% \begin{keywords}
%   example, \LaTeX
% \end{keywords}

% REQUIRED
% \begin{AMS}
%   68Q25, 68R10, 68U05
% \end{AMS}

\section{Introduction}

Diese Ausarbeitung bezieht sich auf den zweiten Teil des Vortrags zu~\cite{PeVo19} und befasst sich mit dem Beweis von~\cite{PeVo19}, Satz 4.1. Sätze und Aussagen, die aus dem ersten Teil des Vortrags verwendet werden, werden ohne Beweis zitiert. 

\section{Main results}

\begin{theorem}
  Sei \(\mathcal{G}\) eine endliche Gruppe, {} \(\epsilon \in [0, \infty)\), {} \(p \in (0, \infty]\) {} und \(C_0, N \in \mathbb{N}\). {} Es sei \(\rho: \mathbb{R} \to \mathbb{R}\) messbar {} und \(\Omega \subseteq \mathbb{R}^{\llbracket C_0\rrbracket  \times \mathcal{G}}\) eine \(\mathcal{G}\)-invariante Menge. {}% chktex 9 
  Sei \(F:\ \mathbb{R}^{\llbracket C_0\rrbracket  \times \mathcal{G}} \to \mathbb{R}^{\llbracket N\rrbracket  \times \mathcal{G}}\) messbar und translationsäquivariant {} und \(\Phi\) ein FNN mit Architektur \(\mathcal{A}(\Phi) = (C_0 \times |\mathcal{G}|, N_1, \dots, N_{L-1}, N)\) sodass \(\|(F)_1 - R_\rho(\Phi)\|_{L^p(\Omega, \mathbb{R}^N)} \le \epsilon\). {}\\ 
  Dann existiert ein CNN \(\Psi\) mit der Anzahl von Kanälen \((C_0, N_1, \dots, N_{L-1}, N)\) und Filtern \((N_1 \cdot C_0, 1, \dots, 1)\) {}sodass \(\|F-R_\rho(\Psi)\|_{L^p(\Omega, \mathbb{R}^{\llbracket N\rrbracket  \times \mathcal{G}})} \le |\mathcal{G}|^{1/p} \cdot \epsilon\) {}und \(\Wc(\Psi) \le 2 \cdot W(\Phi)\). {} Dabei gilt die Konvention \(|G|^{1/\infty} = 1\).
\end{theorem}

\begin{proof}
  Da für ein CNN \(\Psi\) gilt, dass \(R_\rho(\Psi)\) translationsäquivariant ist reicht es nach Bemerkung~\ref{Reicht1zuzeigen} zu zeigen, dass es ein \(\Psi\) mit den geforderten Kriterien gibt sodass \((R_\rho(\Psi))_1 = R_\rho(\Phi)\). Sei \(\Phi = (V_1, \dots, V_L)\). Wir setzen \(N_0 \coloneqq C_0\), \(N_L \coloneqq N\), \(k_1 \coloneqq N_1 \cdot C_0\) und \(k_l = 1\) für alle \(l \in \{2, \dots, L\}\). \\

  Wir betrachten zuerst den Sonderfall \(\|V_L\|_{l^0} = 0\). Dann gilt \(R_\rho(\Phi) = 0\). Setze nun \(\Psi = (T_1, \dots, T_L)\), wobei \(T_l = A_l^\uparrow \circ B_l\) für alle \(l \in \{1, \dots, L\}\) und

  \begin{align*}
    &A_l: \ \mathbb{R}^{\llbracket k_l\rrbracket \times \llbracket N_{l-1}\rrbracket } \longrightarrow \mathbb{R}^{\llbracket N_l\rrbracket }, \ x \longmapsto 0 \\
    &B_l: \ \mathbb{R}^{\llbracket N_{l-1}\rrbracket  \times \mathcal{G}} \longrightarrow \mathbb{R}^{\llbracket k_l\rrbracket  \times \llbracket N_{l-1}\rrbracket  \times \mathcal{G}}, \ (x_i)_{i \in \llbracket N_{l-1}\rrbracket } \longmapsto (x_i \star 0)_{r \in \llbracket k_l\rrbracket , i \in \llbracket N_{l-1}\rrbracket }.
  \end{align*}
  

  Dann besitzt \(\Psi\) die geforderte Anzahl an Filtern und Kanälen, es gilt \((R_\rho(\Psi))_1 = 0\) und \(\|T_l\|_{\lc} = 0\) für alle \(l = 1, \dots, L\), sodass \(\Wc(\Psi)  = 0 \le 2 W(\Phi) \).

  Als nächstes betrachten wir den Fall, dass \(\| V_L \|_{l^0} > 0\), aber \(\|V_l \|_{l^0} = 0\) für ein \(l \in \{1, \dots, L-1\}\). Dann gibt es ein \(c \in \mathbb{R}^N\), sodass \(\| c \|_{l_0} \le W(\Phi)\) und \(R_\rho (\Phi) = c\) und es gibt ein \(c_0 \in R^{N_{L-1}}\) mit \(c = V_Lc_0\). \\
  Für jedes \(A \in \mathbb{R}^{n \times k}, b \in \mathbb{R}^n\) und \(x \in \mathbb{R}^k\)  sehen wir aus \((Ax + b)_j = b_j + \sum_{l=1}^k A_{j, l} x_l\), dass 
  \begin{equation}
  \mathbb{1}_{(Ax + b)_j \ne 0} \le \mathbb{1}_{b_j \ne 0} + \sum_{l=1}^k \mathbb{1}_{A_{j,l} \ne 0}\label{dreicksungleichungIndikator}
  \end{equation}
  und es folgt
  \begin{equation}
    \| Ax + b \|_{l^0} 
    \overset{(\text{Def. }l^0)}{=} \sum_{j=1}^n \mathbb{1}_{(Ax + b)_j \ne 0}  
    \overset{(\ref{dreicksungleichungIndikator})}{\le} \sum_{j=1}^n \mathbb{1}_{b_j \ne 0} + \sum_{j=1}^n \sum_{l=1}^k \mathbb{1}_{A_{j, l} \ne 0}  
    \overset{(\text{Def. }l^0)}{=} \| A(\cdot) + b\|_{l^0}.\label{abschaetzungL0Affine}
  \end{equation}
  
  Also folgt
  \begin{equation}
    \|c\|_{l^0} = \| V_L c_0 \|_{l^0}  
    \overset{(\ref{abschaetzungL0Affine})}{\le} \| V_L \|_{l^0}  
    \le \sum_{l=1}^L \|V_l\|_{l^0}  
    = W(\Phi). \label{cAbschaetzung}
  \end{equation}

  Ist nun so ein Vektor \(c \in \mathbb{R}^N\) mit \(R_\rho(\Phi) = c\) und \(\| c \|_{l^0} \le W(\Phi)\) gegeben, setze \(\Psi = (T_1, \dots, T_L)\), wobei \(T_l = A_l^\uparrow \circ B_l\) mit \(B_1, \dots, B_L\) und \(A_1, \dots, A_{L-1}\) wie im vorherigen Fall und 
  \[A_L: \ \mathbb{R}^{\llbracket k_L \rrbracket \times \llbracket N_{L-1} \rrbracket} \longrightarrow \mathbb{R}^{\llbracket N_L \rrbracket}, \ x \longmapsto c.\]
  Dies ist wohldefiniert da \(N_L = N\) und \(c \in \mathbb{R}^N \simeq \mathbb{R}^{\llbracket N_L \rrbracket}\).  Es folgt \((R_\rho(\Psi))_1 = c = R_\rho(\Phi)\) und \(\Psi\) hat die geforderte Anzahl an Filtern und Kanälen und es gilt \(\Wc(\Psi) = \| A_L \|_{l^0} = \| c \|_{l^0} \overset{(\ref{cAbschaetzung})}{\le} W(\Phi)\).

  Wir können nun \(\|V_l\|_{l^0} > 0\) für alle \(l \in \{ 1, \dots, L\}\) annehmen. Definiere nun \(v_g^\star \coloneqq v_{g^{-1}} \in \mathbb{R}^{\mathcal{G}}\) für \(v \in \mathbb{R}^{\mathcal{G}}\), \(g \in \mathbb{G}\), dann gilt:
  \begin{equation}
    (x \star v^\star)_1
    = \sum_{h \in \mathbb{G}} x_h v_{h^{-1}}^\star 
    = \sum_{h \in \mathbb{G}} x_h v_h 
    = \langle x, v \rangle_{\mathbb{R}^{\mathbb{G}}} 
    \qquad \forall x \in \mathbb{R}^{\mathbb{G}} \label{faltScalarGleich}
  \end{equation} 
  Sei nun \((\delta_1)_g = \begin{cases} 1 &g = 1 \\ 0 & g \in \mathcal{G} \setminus \{1\} \end{cases}\) und \(x \in \mathbb{R}^{\mathcal{G}}\). Dann gilt
  \begin{equation}
  (x \star \delta_1)_g 
  = \left(\sum_{h \in \mathcal{G}} x_h (\delta_1)_{h^{-1}}\right)_g 
  = \sum_{h \in \mathcal{G}} \left(x_h (\delta_1)_{h^{-1}}\right)_g 
  =  x_g (\delta_1)_{g^{-1} g} 
  = x_g, \label{delta1istidentitaet}
  \end{equation}
  also \((x \star \delta_1) = x\).

  Es gelte nach wie vor 
  \(\Phi = (V_1, \dots, V_L)\). Da \(V_1:\ \mathbb{R}^{\llbracket C_0 \rrbracket \times \mathcal{G}} \to \mathbb{R}^{\llbracket N_1 \rrbracket}\) affin linear ist,  gibt es \(v_j^i \in \mathbb{R}^{\mathcal{G}}\) und \(b_j \in \mathbb{R}, j \in \llbracket N_1 \rrbracket, i \in \llbracket C_0 \rrbracket\), sodass \(V_1(\cdot) = b + V_1^{\operatorname{lin}}\), wobei \(b = (b_j)_{j \in \llbracket N_1 \rrbracket}\) und 
  \[V_1^{\operatorname{lin}}: \ \mathbb{R}^{\llbracket C_0 \rrbracket \times \mathcal{G}} \longrightarrow \mathbb{R}^{\llbracket N_1 \rrbracket}, \ (x_i)_{i \in \llbracket C_0 \rrbracket} \longmapsto \left(\sum_{i=1}^{C_0} \langle x_i, v_j^i\rangle_{\mathbb{R}^{\mathcal{G}}} \right)_{j \in \llbracket N_1 \rrbracket}\]
  Definiere nun \(T_1 \coloneqq A_1^\uparrow \circ B_1 \in \opr{Conv}(\mathcal{G}, C_0 \cdot N_1, C_0, N_1)\) mit
  \[B_1: \ \mathbb{R}^{\llbracket{C_0} \rrbracket \times \mathcal{G}} \longrightarrow \mathbb{R}^{\llbracket N_1 \rrbracket \times \llbracket C_0 \rrbracket \times \llbracket C_0 \rrbracket \times \mathcal{G}}, \ (x_i)_{i \in \llbracket C_0 \rrbracket} \longmapsto (x_\iota \star (v_j^i)^\star)_{j \in \bbrack{N_1}, i, \iota \in \bbrack{C_0}}\]
  und \(A_1: \ \mathbb{R}^{\llbracket N_1 \rrbracket \times \llbracket C_0 \rrbracket \times \llbracket C_0 \rrbracket} \longrightarrow \mathbb{R}^{\llbracket N_1 \rrbracket}, \ y \longmapsto b + A_1^{\opr{lin}} y\), wobei
  \[
    A_1^{\opr{lin}}: \ \mathbb{R}^{\bbrack{N_1} \times \bbrack {C_0} \times \bbrack{C_0}} 
    \longrightarrow \mathbb{R}^{\bbrack{N_1}}, \ (y_{j,i,\iota})_{j \in \bbrack{N_1}; i,\iota \in \bbrack{C_0}} 
    \longmapsto \left( \sum_{i=1}^{C_0} \mathbb{1}_{v_l^i \ne 0} \cdot y_{l, i, i} \right)_{l \in \bbrack{N_1}}.
  \] 

  Sei nun \(x = (x_{j, g})_{j \in \bbrack{C_0}, g \in \mathcal{G}} \in \mathbb{R}^{\bbrack{C_0} \times \mathcal{G}}\) und \(l \in \bbrack{N_1}\), dann:

  \begin{align*}
    (T_1 x)_{l, 1}
    = ((A_1^\uparrow \circ B_1)x)_{l, 1} 
    &= \left(A_1 \left[(B_1 x)_{j, i, \iota, 1}\right]_{(j, i, \iota) \in \bbrack{N_1} \times \bbrack{C_0} \times \bbrack{C_0}}\right)_l = \left( A_1 \left[ (x_\iota \star (v_j^i)^\star_1 \right]_{(j, i, \iota) \in \bbrack{N_1} \times \bbrack{C_0} \times \bbrack{C_0}} \right)_l  \\ % chktex 9
    \text{(Def.~von \(A_1\) und Gleichung~\ref{faltScalarGleich})} \quad 
    &= b_l + \sum_{i=1}^{C_0} \left[ \langle x_i, v_l^i\rangle_{\mathbb{R}^{\mathcal{G}}} \cdot \mathbb{1}_{v_l^i \ne 0} \right] = b_l + \sum_{i=1}^{C_0} \langle x_i, v_l^i \rangle_{\mathbb{R}^{\mathcal{G}}} 
    = (V_1 x)_l
  \end{align*}
  
  Mit der Definition der Projektionsabbildung \(\pi_1^{\bbrack{N_1}}\) (Notation~\ref{definitionProjektion}) erhalten wir:
  \begin{equation}
    \pi_1^{\bbrack{N_1}} \circ T_1 = V_1 \label{T1projeziertIstV1}
  \end{equation}

  Dann gilt für alle \(j, l \in \bbrack{N_1}\) und \(i, \iota \in \bbrack{C_0}\): 

  \begin{equation}
    (A_1^{\opr{lin}} \delta_{j, i, \iota})_l = \sum_{n=1}^{C_0} (\mathbb{1}_{v_l^n \ne 0} \cdot \delta_{j, i, \iota})_{l, n, n} =  \Delta_{i, \iota} \cdot \mathbb{1}_{v_j^i \ne 0} \cdot \Delta_{j, l}, \quad \text{wobei }\Delta_{i,j} \coloneqq \begin{cases} 1, &i = j \\ 0, &i \ne j \end{cases}\label{einhsvecundkronecker}
  \end{equation} 

  Also gilt:
  \begin{align}
    \| A_1^{\opr{lin}} \|_{l^0} 
    \overset{\text{(Def. \(l^0\)})}{=} \sum_{j=1}^{N_1} \sum_{l=1}^{N_1} \sum_{i, \iota \in \bbrack{C_0}} \mathbb{1}_{(A_1^{\opr{lin}} \delta_{j, i, \iota})_l \ne 0} 
    \overset{(\ref{einhsvecundkronecker})}{=} \sum_{j=1}^{N_1} \sum_{i=1}^{C_0} \mathbb{1}_{v_j^i \ne 0} 
    \le \sum_{j=1}^{N_1} \sum_{j=1}^{C_0} \| v_j^i \|_{l^0} 
    \overset{\text{(Def. \(l^0\)})}{=} \| V_1^{\opr{lin}} \|_{l^0} \label{A1lingeV1lin}
  \end{align}

  und es folgt
  \begin{align}
    \|A_1\|_{l^0} 
    \overset{\text{(Def. \(A_1\)})}{=} \|b + A_1^{\opr{lin}} y\|_{l^0} 
    \overset{\text{(Def. \(l^0\)})}{=} \|b\|_{l^0} + \|A_1^{\opr{lin}}\|_{l^0} 
    \overset{(\ref{A1lingeV1lin})}{\le} \|b\|_{l^0} + \|V_1^{\opr{lin}}\|_{l^0} 
    \overset{\text{(Def. \(l^0\)})}{=} \|b + V_1^{\opr{lin}}\|_{l^0} \overset{\text{(Def. \(V_1\)})}{=} \|V_1\|_{l^0} \label{A1leV1}
  \end{align}

  Als nächstes stellen wir fest, dass 
  \begin{equation}
    \|B_1\|_{l_{\opr{filter}}^0} 
    \overset{\text{(Def. \(l^0_{\opr{filter}}\))}}{=} \sum_{j=1}^{N_1} \sum_{i=1}^{C_0} \|(v_j^i)^\star\|_{l^0} 
    \overset{\text{(Def. \(l^0\)})}{=} \|V_1^{\opr{lin}}\|_{l^0} 
    \le \|b + V_1^{\opr{lin}}(\cdot)\|_{l^0} 
    = \|V_1\|_{l^0}.\label{B1leV1}
  \end{equation} 

  Dies impliziert
  \begin{equation}
    \|T_1\|_{l_{\opr{conv}}^0} 
    \overset{(\|\cdot\|_{l^0_{\opr{conv}}} = \min \{ \dots \})}{\le} \|A_1\|_{l^0} + \|B_1\|_{l_{\opr{filter}}^0} 
    \overset{(\ref{A1leV1}) (\ref{B1leV1})}{\le} 2 \cdot \|V_1\|_{l^0}. \label{T1leV1}
  \end{equation}

  Für \(l \in \{2, \dots, L\}\) definiere nun \(T_l \coloneqq V_l^\uparrow \circ B_l \in \opr{Conv}(\mathcal{G}, 1, N_{l-1}, N_l)\) mit
  \[
    B_l: \ \mathbb{R}^{\bbrack{N_{l-1}} \times \mathcal{G}} \longmapsto \mathbb{R}^{\bbrack{N_{l-1}} \times \mathcal{G}}, \ 
    x = (x_i)_{i \in \bbrack{N_{l-1}}} \longmapsto (x_i \star \delta_1)_{i \in \bbrack{N_{l-1}}} \overset{(\ref{delta1istidentitaet})}{=} x.
  \]

  Wegen \(B_l x = x\) gilt \(T_l = V_l^\uparrow\). Da wir den Fall \(\|V_l\|_{l^0} \ne 0\) betrachten gilt \(\|B_l\|_{l_{\opr{filter}}^0} \overset{(\text{Def. }l_{\opr{filter}}^0)}{=} \|\delta_1\|_{l^0} = 1 \le ||V_l\|_{l^0}\).

  \vfill

  Wir können nun das CNN \(\Psi \coloneqq (T_1, \dots, T_L)\) definieren. Dann hat \(\Psi\) die Anzahl von Kanälen \((C_0, N_1, \dots, N_{L-1}, N)\) und Filtern \((N_1 \cdot C_0, 1, \dots, 1)\) und es gilt
  \[
    \Wc (\Psi) 
    \overset{(\text{Def. }\Wc)}{=} \sum_{l=1}^L \| T_l \|_{l_{\opr{conv}}^0} 
    \overset{(\ref{T1leV1})}{\le} 2 \sum_{l=1}^L \|V_l\|_{l^0} 
    \overset{(\text{Def. \(W\)})}{=} 2 \cdot W(\Phi)
  \]


  Nach Satz~\ref{CnnRealisierungTranslqvariant} ist \(R_{\rho}(\Psi)\) translationsäquivariant. Da \(F\) nach Voraussetzung auch translationsäquivariant ist, genügt es nach Bemerkung~\ref{translquvrtReichtEinPUnkt} zu zeigen, dass \([R_\rho(\Psi)]_1 = R_\rho(\Phi)\). Wir haben bereits gezeigt, dass \(T_l = V_l^\uparrow\) für \(l \in \{2, \dots, L\}\). Hieraus folgt 
  \[
    \pi_1^{\bbrack{N_l}} \circ T_l 
    = \pi_1^{\bbrack{N_L}} \circ V_l^\uparrow 
    = V_l \circ \pi_1^{\bbrack{N_{l-1}}}.
  \] 

  Da die Aktivierungsfunktion \(\rho\) komponentenweise angewandt wird können wir schließen:
  \begin{align*}
    [R_\rho(\Psi)]_1 
    &= \pi_1^{\bbrack{N_L}} \circ R_\rho(\Psi)  \\
    &= (V_L \circ \rho \circ V_{L-1} \circ \dots \rho \circ V_2) \circ \pi_1^{\bbrack{N_1}} \circ \rho \circ T_1 \\ 
    \text{(Gleichung~\ref{T1projeziertIstV1} und \(\pi_1^{\bbrack{N_1}}\circ \rho = \rho \circ \pi_1^{\bbrack{N_1}}\))}  \quad 
    &= (V_L \circ \rho \circ V_{L-1} \circ \dots \circ \rho \circ V_2) \circ \rho \circ V_1 \\ 
    &= R_\rho(V_1, \dots, V_L)  \\
    &= R_\rho(\Phi)
  \end{align*}

  Hieraus folgt die Behauptung.  \qed\

\end{proof}


\bibliographystyle{siamplain}
\bibliography{references}
\end{document}
